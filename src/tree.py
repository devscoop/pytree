import os

# Open as python tree.py and enter loc
class TreeView:
	def __init__(self):
		print "Please enter the folder location : "
		self.db_loc = raw_input()
		#get list of files in the directory
		#get list of each file in that directory
		#and so forth
		self.indent = -1
		self.full = ""
		print "This may take a while.."
		self.FILE = open("list.txt","w")
		self.deep_search(self.db_loc,0)	
		self.FILE.close()
		
	def deep_search(self,path,indent):
		stri = ""
		flag = 0
		try:
			flist = os.listdir(path)
			#is a folder
		except:
			print "file detected!" + path
			#is not a folder
			flag = 1 #flag it
		if flag == 0:
			indent+=1
			i = indent
			for f in flist:
				while i>0:
					stri+="-"
					i-=1
				#for each file in that folder
				text = stri+f
				#print text
				self.FILE.writelines("\n"+text)
				newpath = path + "/" + f
				self.deep_search(newpath,indent)
	
TreeView()